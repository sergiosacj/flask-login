Source: flask-login
Maintainer: Debian Python Team <team+python@tracker.debian.org>
Uploaders:
 Daniele Tricoli <eriol@debian.org>,
Section: python
Priority: optional
Build-Depends:
 debhelper-compat (= 13),
 dh-sequence-python3,
 dh-sequence-sphinxdoc,
 pybuild-plugin-pyproject,
 python-flask-doc <!nodoc>,
 python3-all,
 python3-blinker <!nocheck>,
 python3-doc <!nodoc>,
 python3-flask <!nodoc>,
 python3-pytest <!nocheck>,
 python3-semantic-version <!nocheck>,
 python3-setuptools,
 python3-sphinx <!nodoc>,
Standards-Version: 4.6.2
Homepage: https://github.com/maxcountryman/flask-login
Vcs-Git: https://salsa.debian.org/python-team/packages/flask-login.git
Vcs-Browser: https://salsa.debian.org/python-team/packages/flask-login
Testsuite: autopkgtest-pkg-python
Rules-Requires-Root: no

Package: python-flask-login-doc
Architecture: all
Section: doc
Depends:
 ${misc:Depends},
 ${sphinxdoc:Depends},
Enhances:
 python3-flask-login,
Breaks:
 python3-flask-login-doc,
Replaces:
 python3-flask-login-doc,
Multi-Arch: foreign
Description: user session management for Flask -- documentation
 Flask-Login provides user session management for Flask. It handles the
 common tasks of logging in, logging out, and remembering your users'
 sessions over extended periods of time.
 .
 This package provides the documentation.

Package: python3-flask-login
Architecture: all
Depends:
 ${misc:Depends},
 ${python3:Depends},
Suggests:
 python-flask-login-doc,
Description: user session management for Flask -- Python 3 module
 Flask-Login provides user session management for Flask. It handles the
 common tasks of logging in, logging out, and remembering your users'
 sessions over extended periods of time.
 .
 Flask-Login is not bound to any particular database system or permissions
 model. The only requirement is that your user objects implement a few
 methods, and that you provide a callback to the extension capable of
 loading users from their ID.
 .
 This package provides the Python 3 module.
